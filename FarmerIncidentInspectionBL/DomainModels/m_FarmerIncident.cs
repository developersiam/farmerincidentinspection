﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.DomainModels
{
    public class m_FarmerIncident
    {
        public string CategoryName { get; set; }
        public string TopicTitle { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> IncidentDate { get; set; }
        public Nullable<System.DateTime> FollowUpDate { get; set; }
        public string CitizenID { get; set; }
        public short Crop { get; set; }
    }
}
