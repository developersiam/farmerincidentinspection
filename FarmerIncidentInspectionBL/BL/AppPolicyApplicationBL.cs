﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IApplicationBL
    {
        void Add(AppPolicyApplication model);
        void Edit(AppPolicyApplication model);
        void Delete(Guid applicationId);
        AppPolicyApplication GetSingle(Guid applicationId);
        List<AppPolicyApplication> GetAll();
    }

    public class AppPolicyApplicationBL : IApplicationBL
    {
        IStecDBMSUnitOfWork _uow;
        public AppPolicyApplicationBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public void Add(AppPolicyApplication model)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyApplicationValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = _uow.AppPolicyApplicationRepository.Get();
                if (list.Where(x => x.ApplicationName == model.ApplicationName).Count() > 0)
                    throw new ArgumentException("มีชื่อ applicatoin นี้แล้วในระบบ");

                model.ApplicationID = Guid.NewGuid();
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                _uow.AppPolicyApplicationRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid applicationId)
        {
            try
            {
                var model = _uow.AppPolicyApplicationRepository
                    .GetSingle(x => x.ApplicationID == applicationId, 
                    x => x.AppPolicyRoles);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.AppPolicyRoles.Count() > 0)
                    throw new ArgumentException("มีการสร้าง Role ภายใต้ application นี้แล้ว ไม่สามารถลบข้อมูลนี้ได้");

                _uow.AppPolicyApplicationRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(AppPolicyApplication model)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyApplicationValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.ApplicationID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.ModifiedDate = DateTime.Now;

                _uow.AppPolicyApplicationRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyApplication> GetAll()
        {
            return _uow.AppPolicyApplicationRepository.Get().ToList();
        }

        public AppPolicyApplication GetSingle(Guid applicationId)
        {
            return _uow.AppPolicyApplicationRepository.GetSingle(x => x.ApplicationID == applicationId);
        }
    }
}
