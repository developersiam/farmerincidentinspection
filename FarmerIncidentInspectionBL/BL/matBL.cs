﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface ImatBL
    {
        mat GetSingle(string bc);
        List<mat> GetByToPdno(string topdno);
    }

    public class matBL : ImatBL
    {
        IStecDBMSUnitOfWork _uow;
        public matBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public List<mat> GetByToPdno(string topdno)
        {
            return _uow.matRepository.Get(x => x.topdno == topdno).ToList();
        }

        public mat GetSingle(string bc)
        {
            return _uow.matRepository.GetSingle(x => x.bc == bc);
        }
    }
}
