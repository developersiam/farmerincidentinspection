﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IpackedgradeBL
    {
        List<packedgrade> GetAll();
        List<packedgrade> GetByCrop(short crop);
        List<packedgrade> GetByType(short crop, string type);
        List<packedgrade> GetByCustomer(short crop, string customer);
        packedgrade GetSingle(string packedgrade);

    }

    public class packedgradeBL : IpackedgradeBL
    {
        IStecDBMSUnitOfWork _uow;
        public packedgradeBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public List<packedgrade> GetAll()
        {
            return _uow.packedgradeRepository.Get().ToList();
        }

        public List<packedgrade> GetByCrop(short crop)
        {
            return _uow.packedgradeRepository.Get(x => x.crop == crop).ToList();
        }

        public List<packedgrade> GetByCustomer(short crop, string customer)
        {
            return _uow.packedgradeRepository.Get(x => x.crop == crop && x.customer == customer).ToList();
        }

        public List<packedgrade> GetByType(short crop, string type)
        {
            return _uow.packedgradeRepository.Get(x => x.crop == crop && x.type == type).ToList();
        }

        public packedgrade GetSingle(string packedgrade)
        {
            return _uow.packedgradeRepository.GetSingle(x => x.packedgrade1 == packedgrade);
        }
    }
}
