﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IInspectionBL
    {
        void Add(FarmerIncidentInspection model);
        void Update(FarmerIncidentInspection model);
        void Delete(string baleBarcode);
        FarmerIncidentInspection GetSingle(string baleBarcode);
        List<FarmerIncidentInspection> GetByPdno(string pdno);
    }

    public class InspectionBL : IInspectionBL
    {
        IStecDBMSUnitOfWork _uow;
        public InspectionBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public void Add(FarmerIncidentInspection model)
        {
            try
            {
                var validation = new DomainValidation.IsInspectionValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.RecordDate = DateTime.Now;

                _uow.FarmerIncidentInspectionRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string baleBarcode)
        {
            try
            {
                var model = GetSingle(baleBarcode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.FarmerIncidentInspectionRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerIncidentInspection> GetByPdno(string pdno)
        {
            return _uow.FarmerIncidentInspectionRepository.Get(x => x.pdno == pdno).ToList();
        }

        public FarmerIncidentInspection GetSingle(string baleBarcode)
        {
            return _uow.FarmerIncidentInspectionRepository.GetSingle(x => x.BaleBarcode == baleBarcode);
        }

        public void Update(FarmerIncidentInspection model)
        {
            _uow.FarmerIncidentInspectionRepository.Update(model);
            _uow.Save();
        }
    }
}
