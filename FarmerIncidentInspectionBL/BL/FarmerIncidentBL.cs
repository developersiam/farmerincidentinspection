﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionBL.DomainModels;
using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionDAL;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IFarmerIncidentBL
    {
        List<sp_GetIncidentFarmerByBaleBarcode_Result> GetFromStoreByBaleBarcode(string baleBarcode);
        List<m_FarmerIncident> GetByBaleBarcode(string baleBarcode);
    }

    public class FarmerIncidentBL : IFarmerIncidentBL
    {
        IBUBuyingSystemUnitOfWork _uow;
        public FarmerIncidentBL()
        {
            _uow = new BUBuyingSystemUnitOfWork();
        }

        public List<m_FarmerIncident> GetByBaleBarcode(string baleBarcode)
        {
            StoreProcedureRepository resource = new StoreProcedureRepository();
            return resource.sp_GetIncidentFarmerByBaleBarcode(baleBarcode)
                .Select(x => new m_FarmerIncident
                {
                    CategoryName = x.CategoryName,
                    TopicTitle = x.TopicTitle,
                    Description = x.Description,
                    IncidentDate = x.IncidentDate,
                    FollowUpDate = x.FollowUpDate,
                    CitizenID = x.CitizenID,
                    Crop = x.Crop
                }).ToList();
        }

        public List<sp_GetIncidentFarmerByBaleBarcode_Result> GetFromStoreByBaleBarcode(string baleBarcode)
        {
            StoreProcedureRepository resource = new StoreProcedureRepository();
            return resource.sp_GetIncidentFarmerByBaleBarcode(baleBarcode)
                .Select(x => new sp_GetIncidentFarmerByBaleBarcode_Result
                {
                    CategoryName = x.CategoryName,
                    TopicTitle = x.TopicTitle,
                    Description = x.Description,
                    IncidentDate = x.IncidentDate,
                    FollowUpDate = x.FollowUpDate,
                    CitizenID = x.CitizenID,
                    Crop = x.Crop
                }).ToList();
        }
    }
}
