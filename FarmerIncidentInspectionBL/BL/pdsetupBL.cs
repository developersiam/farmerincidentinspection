﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IpdsetupBL
    {
        pdsetup GetSingle(string pdno);
        pdsetup GetByDefault();
        List<pdsetup> GetByCrop(short crop);
        List<pdsetup> GetByPackGrade(string packedgrade);
    }

    public class pdsetupBL : IpdsetupBL
    {
        IStecDBMSUnitOfWork _uow;
        public pdsetupBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public List<pdsetup> GetByCrop(short crop)
        {
            return _uow.pdsetupRepository.Get(x => x.crop == crop).ToList();
        }

        public pdsetup GetByDefault()
        {
            return _uow.pdsetupRepository.GetSingle(x => x.def == true);
        }

        public List<pdsetup> GetByPackGrade(string packedgrade)
        {
            return _uow.pdsetupRepository.Get(x => x.packedgrade == packedgrade).ToList();
        }

        public pdsetup GetSingle(string pdno)
        {
            return _uow.pdsetupRepository.GetSingle(x => x.pdno == pdno);
        }
    }
}
