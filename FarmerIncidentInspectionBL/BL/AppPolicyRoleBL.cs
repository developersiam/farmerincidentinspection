﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionDAL.UnitOfWork;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IRoleBL
    {
        void Add(AppPolicyRole model);
        void Edit(AppPolicyRole model);
        void Delete(Guid id);
        AppPolicyRole GetSingle(Guid id);
        List<AppPolicyRole> GetAll(Guid applicationId);
    }

    public class AppPolicyRoleBL : IRoleBL
    {
        IStecDBMSUnitOfWork _uow;
        public AppPolicyRoleBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public void Add(AppPolicyRole model)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyRoleValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (_uow.AppPolicyRoleRepository
                    .Get(x => x.RoleName == model.RoleName && x.ApplicationID == model.ApplicationID)
                    .Count() > 0)
                    throw new ArgumentException("role " + model.RoleName + " นี้มีซ้ำแล้วในระบบ");

                model.RoleID = Guid.NewGuid();
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = model.CreateBy;

                _uow.AppPolicyRoleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.AppPolicyUserRoles.Count() > 0)
                    throw new ArgumentException("Role นี้ถูกกำหนดให้ user บางรายไปแล้ว โปรดลบข้อมูลการกำหนดสิทธิ์ออก่กอน");

                _uow.AppPolicyRoleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(AppPolicyRole model)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyRoleValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.RoleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.RoleName = model.RoleName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                _uow.AppPolicyRoleRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyRole> GetAll(Guid applicationId)
        {
            return _uow.AppPolicyRoleRepository.Get(x => x.ApplicationID == applicationId).ToList();
        }

        public AppPolicyRole GetSingle(Guid id)
        {
            return _uow.AppPolicyRoleRepository.GetSingle(x => x.RoleID == id);
        }
    }
}
