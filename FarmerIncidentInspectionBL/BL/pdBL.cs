﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IpdBL
    {
        List<pd> GetByPackedgrade(string grade);
        List<pd> GetByFromPdno(string frompdno);
        List<pd> GetByFromPdhour(string frompdno, short frompdhour);
        List<pd> GetByCustomer(short crop, string customer);
        pd GetBySingle(string bc);
    }

    public class pdBL : IpdBL
    {
        IStecDBMSUnitOfWork _uow;
        public pdBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public List<pd> GetByCustomer(short crop, string customer)
        {
            return _uow.pdRepository.Get(x => x.crop == crop && x.customer == customer).ToList();
        }

        public List<pd> GetByFromPdhour(string frompdno, short frompdhour)
        {
            return _uow.pdRepository.Get(x => x.frompdno == frompdno && x.frompdhour == frompdhour).ToList();
        }

        public List<pd> GetByFromPdno(string frompdno)
        {
            return _uow.pdRepository.Get(x => x.frompdno == frompdno).ToList();
        }

        public List<pd> GetByPackedgrade(string grade)
        {
            return _uow.pdRepository.Get(x => x.grade == grade).ToList();
        }

        public pd GetBySingle(string bc)
        {
            return _uow.pdRepository.GetSingle(x => x.bc == bc);
        }
    }
}
