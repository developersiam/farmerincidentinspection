﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IUserBL
    {
        void Add(AppPolicyUser model);
        void Delete(string username);
        AppPolicyUser GetSingle(string username);
        List<AppPolicyUser> GetAll();
    }

    public class AppPolicyUserBL : IUserBL
    {
        IStecDBMSUnitOfWork _uow;
        public AppPolicyUserBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }

        public void Add(AppPolicyUser model)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyUserValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                if (_uow.AppPolicyUserRepository
                    .Get(x => x.Username == model.Username)
                    .Count() > 0)
                    throw new ArgumentException("username " + model.Username + " นี้มีซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;

                _uow.AppPolicyUserRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username)
        {
            try
            {
                var model = GetSingle(username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.AppPolicyUserRoles.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบผู้ใช้นี้ได้เนื่องจากยังมีข้อมูลการกำหนดสิทธิ์การใช้งานระบบอยู่ โปรดลบสิทธิ์การใช้งานระบบออกก่อน");

                _uow.AppPolicyUserRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyUser> GetAll()
        {
            return _uow.AppPolicyUserRepository.Get().ToList();
        }

        public AppPolicyUser GetSingle(string username)
        {
            return _uow.AppPolicyUserRepository
                    .GetSingle(x => x.Username == username);
        }
    }
}
