﻿using FarmerIncidentInspectionDAL.UnitOfWork;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.BL
{
    public interface IUserRoleBL
    {
        void Add(AppPolicyUserRole model, Guid applicationId);
        void Delete(string username, Guid roleID);
        AppPolicyUserRole GetSingle(string username, Guid roleID);
        List<AppPolicyUserRole> GetAll();
        List<AppPolicyUserRole> GetByUsername(string username, Guid applicationId);
    }

    public class AppPolicyUserRoleBL : IUserRoleBL
    {
        IStecDBMSUnitOfWork _uow;
        public AppPolicyUserRoleBL()
        {
            _uow = new StecDBMSUnitOfWork();
        }
        public void Add(AppPolicyUserRole model, Guid applicationId)
        {
            try
            {
                var validation = new DomainValidation.IsAppPolicyUserRoleValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = GetByUsername(model.Username, applicationId);
                if (list.Where(x => x.RoleID == model.RoleID).Count() > 0)
                    throw new ArgumentException("มีการกำหนด role นี้ซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;

                _uow.AppPolicyUserRoleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleID)
        {
            try
            {
                var model = GetSingle(username, roleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.AppPolicyUserRoleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyUserRole> GetAll()
        {
            return _uow.AppPolicyUserRoleRepository
                .Get(null, null, x => x.AppPolicyRole)
                .ToList();
        }

        public List<AppPolicyUserRole> GetByUsername(string username, Guid applicationId)
        {
            return _uow.AppPolicyUserRoleRepository
                .Get(x => x.Username == username &&
                x.AppPolicyRole.ApplicationID == applicationId,
                null,
                x => x.AppPolicyRole)
                .ToList();
        }

        public AppPolicyUserRole GetSingle(string username, Guid roleID)
        {
            return _uow.AppPolicyUserRoleRepository
                .GetSingle(x => x.Username == username && x.RoleID == roleID);
        }
    }
}
