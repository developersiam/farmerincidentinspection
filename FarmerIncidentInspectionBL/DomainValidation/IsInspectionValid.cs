﻿using DomainValidation.Validation;
using FarmerIncidentInspectionBL.DomainValidation.Expression;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.DomainValidation
{
    public class IsInspectionValid : Validator<FarmerIncidentInspection>
    {
        public IsInspectionValid()
        {

            Add("BaleBarcodeIsNotNullOrWhiteSpace", new Rule<FarmerIncidentInspection>(new IsNotNull<FarmerIncidentInspection>(x => x.BaleBarcode), "BaleBarcode field is missing"));
            Add("pdnoIsNotNullOrWhiteSpace", new Rule<FarmerIncidentInspection>(new IsNotNull<FarmerIncidentInspection>(x => x.pdno), "pdno field is missing"));
            Add("RecordByIsNotNullOrWhiteSpace", new Rule<FarmerIncidentInspection>(new IsNotNull<FarmerIncidentInspection>(x => x.RecordBy), "RecordBy field is missing"));
            Add("RecordDateIsNotNullOrWhiteSpace", new Rule<FarmerIncidentInspection>(new IsNotNull<FarmerIncidentInspection>(x => x.RecordDate), "RecordDate field is missing"));
        }
    }
}
