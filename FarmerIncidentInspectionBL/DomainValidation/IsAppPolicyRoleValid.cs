﻿using DomainValidation.Validation;
using FarmerIncidentInspectionBL.DomainValidation.Expression;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.DomainValidation
{
    public class IsAppPolicyRoleValid : Validator<AppPolicyRole>
    {
        public IsAppPolicyRoleValid()
        {
            Add("RoleNameIsNotNullOrWhiteSpace", new Rule<AppPolicyRole>(new IsNotNull<AppPolicyRole>(x => x.RoleName), "RoleName field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<AppPolicyRole>(new IsNotNull<AppPolicyRole>(x => x.CreateBy), "CreateBy field is missing"));
            Add("CreateDateIsNotNullOrWhiteSpace", new Rule<AppPolicyRole>(new IsNotNull<AppPolicyRole>(x => x.CreateDate), "CreateDate field is missing"));
        }
    }
}
