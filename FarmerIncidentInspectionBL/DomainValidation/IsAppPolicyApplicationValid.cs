﻿using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionBL.DomainValidation.Expression;

namespace FarmerIncidentInspectionBL.DomainValidation
{
    public class IsAppPolicyApplicationValid : Validator<AppPolicyApplication>
    {
        public IsAppPolicyApplicationValid()
        {
            Add("ApplicationNameIsNotNullOrWhiteSpace", new Rule<AppPolicyApplication>(new IsNotNull<AppPolicyApplication>(x => x.ApplicationName), "ApplicationName field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<AppPolicyApplication>(new IsNotNull<AppPolicyApplication>(x => x.CreateBy), "CreateBy field is missing"));
            Add("CreateDateIsNotNullOrWhiteSpace", new Rule<AppPolicyApplication>(new IsNotNull<AppPolicyApplication>(x => x.CreateDate), "CreateDate field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<AppPolicyApplication>(new IsNotNull<AppPolicyApplication>(x => x.ModifiedBy), "ModifiedBy field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<AppPolicyApplication>(new IsNotNull<AppPolicyApplication>(x => x.ModifiedDate), "ModifiedDate field is missing"));
        }
    }
}
