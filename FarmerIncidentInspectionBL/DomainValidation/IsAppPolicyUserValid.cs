﻿using DomainValidation.Validation;
using FarmerIncidentInspectionBL.DomainValidation.Expression;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.DomainValidation
{
    public class IsAppPolicyUserValid : Validator<AppPolicyUser>
    {
        public IsAppPolicyUserValid()
        {
            Add("UsernameIsNotNullOrWhiteSpace", new Rule<AppPolicyUser>(new IsNotNull<AppPolicyUser>(x => x.Username), "Username field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<AppPolicyUser>(new IsNotNull<AppPolicyUser>(x => x.CreateBy), "CreateBy field is missing"));
            Add("CreateDateIsNotNullOrWhiteSpace", new Rule<AppPolicyUser>(new IsNotNull<AppPolicyUser>(x => x.CreateDate), "CreateDate field is missing"));
        }
    }
}
