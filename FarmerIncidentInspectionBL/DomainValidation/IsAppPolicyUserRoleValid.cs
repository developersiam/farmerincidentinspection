﻿using DomainValidation.Validation;
using FarmerIncidentInspectionBL.DomainValidation.Expression;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL.DomainValidation
{
    public class IsAppPolicyUserRoleValid : Validator<AppPolicyUserRole>
    {
        public IsAppPolicyUserRoleValid()
        {
            Add("UsernameIsNotNullOrWhiteSpace", new Rule<AppPolicyUserRole>(new IsNotNull<AppPolicyUserRole>(x => x.Username), "Username field is missing"));
            Add("RoleIDIsNotNullOrWhiteSpace", new Rule<AppPolicyUserRole>(new IsNotNull<AppPolicyUserRole>(x => x.RoleID), "RoleID field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<AppPolicyUserRole>(new IsNotNull<AppPolicyUserRole>(x => x.CreateBy), "CreateBy field is missing"));
            Add("CreateDateIsNotNullOrWhiteSpace", new Rule<AppPolicyUserRole>(new IsNotNull<AppPolicyUserRole>(x => x.CreateDate), "CreateDate field is missing"));
        }
    }
}
