﻿using FarmerIncidentInspectionBL.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionBL
{
    public static class BusinessLayerServices
    {
        public static ImatBL matBL()
        {
            ImatBL obj = new matBL();
            return obj;
        }
        public static IpdBL pdBL()
        {
            IpdBL obj = new pdBL();
            return obj;
        }
        public static IpdsetupBL pdsetupBL()
        {
            IpdsetupBL obj = new pdsetupBL();
            return obj;
        }
        public static IpackedgradeBL packedgradeBL()
        {
            IpackedgradeBL obj = new packedgradeBL();
            return obj;
        }
        public static IFarmerIncidentBL FarmerIncidentBL()
        {
            IFarmerIncidentBL obj = new FarmerIncidentBL();
            return obj;
        }
        public static IInspectionBL InspectionBL()
        {
            IInspectionBL obj = new InspectionBL();
            return obj;
        }
        public static IUserBL UserBL()
        {
            IUserBL obj = new AppPolicyUserBL();
            return obj;
        }
        public static IRoleBL RoleBL()
        {
            IRoleBL obj = new AppPolicyRoleBL();
            return obj;
        }
        public static IUserRoleBL UserRoleBL()
        {
            IUserRoleBL obj = new AppPolicyUserRoleBL();
            return obj;
        }
        public static IApplicationBL ApplicationBL()
        {
            IApplicationBL obj = new AppPolicyApplicationBL();
            return obj;
        }
    }
}
