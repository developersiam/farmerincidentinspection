﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarmerIncidentInspectionDAL.EDMX;
using FarmerIncidentInspectionEntities;

namespace FarmerIncidentInspectionDAL
{
    public interface IStoreProcedureRepository
    {
        List<sp_GetIncidentFarmerByBaleBarcode_Result> sp_GetIncidentFarmerByBaleBarcode(string baleBarcode);
    }

    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public List<sp_GetIncidentFarmerByBaleBarcode_Result> sp_GetIncidentFarmerByBaleBarcode(string baleBarcode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetIncidentFarmerByBaleBarcode(baleBarcode).ToList();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}