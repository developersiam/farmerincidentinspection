﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FarmerIncidentInspectionDAL
{
    public interface IGenericDataRepository<T> where T : class
    {
        List<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes);
        IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] include);
        T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] include);
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);
        //IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        //IList<T> GetList(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        //T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties);
        //void Add(params T[] items);
        //void Update(params T[] items);
        //void Remove(params T[] items);
    }
}
