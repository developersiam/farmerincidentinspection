﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FarmerIncidentInspectionDAL.EDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    using FarmerIncidentInspectionEntities;

    public partial class BuyingSystemEntities : DbContext
    {
        public BuyingSystemEntities()
            : base("name=BuyingSystemEntities")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual ObjectResult<sp_GetIncidentFarmerByBaleBarcode_Result> sp_GetIncidentFarmerByBaleBarcode(string baleBarcode)
        {
            var baleBarcodeParameter = baleBarcode != null ?
                new ObjectParameter("baleBarcode", baleBarcode) :
                new ObjectParameter("baleBarcode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetIncidentFarmerByBaleBarcode_Result>("sp_GetIncidentFarmerByBaleBarcode", baleBarcodeParameter);
        }
    }
}
