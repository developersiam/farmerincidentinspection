﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FarmerIncidentInspectionDAL.EDMX;
using FarmerIncidentInspectionEntities;

namespace FarmerIncidentInspectionDAL.UnitOfWork
{
    public class BUBuyingSystemUnitOfWork : IBUBuyingSystemUnitOfWork, System.IDisposable
    {
        private readonly BuyingSystemEntities _context;
        private IGenericDataRepository<sp_GetIncidentFarmerByBaleBarcode_Result> _spIncidentFarmerRepository;

        public BUBuyingSystemUnitOfWork()
        {
            _context = new BuyingSystemEntities();
        }

        public IGenericDataRepository<sp_GetIncidentFarmerByBaleBarcode_Result> spIncidentFarmerRepository
        {
            get
            {
                return _spIncidentFarmerRepository ?? (_spIncidentFarmerRepository = new GenericDataRepository<sp_GetIncidentFarmerByBaleBarcode_Result>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}