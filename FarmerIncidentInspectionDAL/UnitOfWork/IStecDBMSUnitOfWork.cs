﻿using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionDAL.UnitOfWork
{
    public interface IStecDBMSUnitOfWork
    {
        IGenericDataRepository<mat> matRepository { get; }
        IGenericDataRepository<pd> pdRepository { get; }
        IGenericDataRepository<pdsetup> pdsetupRepository { get; }
        IGenericDataRepository<packedgrade> packedgradeRepository { get; }
        IGenericDataRepository<FarmerIncidentInspection> FarmerIncidentInspectionRepository { get; }
        IGenericDataRepository<AppPolicyApplication> AppPolicyApplicationRepository { get; }
        IGenericDataRepository<AppPolicyUser> AppPolicyUserRepository { get; }
        IGenericDataRepository<AppPolicyRole> AppPolicyRoleRepository { get; }
        IGenericDataRepository<AppPolicyUserRole> AppPolicyUserRoleRepository { get; }

        void Save();
    }
}
