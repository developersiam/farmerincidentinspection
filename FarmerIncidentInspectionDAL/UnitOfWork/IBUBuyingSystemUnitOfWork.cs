﻿using FarmerIncidentInspectionDAL.EDMX;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionDAL.UnitOfWork
{
    public interface IBUBuyingSystemUnitOfWork
    {
        IGenericDataRepository<sp_GetIncidentFarmerByBaleBarcode_Result> spIncidentFarmerRepository { get; }
        void Save();
    }
}
