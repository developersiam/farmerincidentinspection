﻿using FarmerIncidentInspectionDAL.EDMX;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionDAL.UnitOfWork
{
    public class StecDBMSUnitOfWork : IStecDBMSUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;
        private IGenericDataRepository<mat> _matRepository;
        private IGenericDataRepository<pd> _pdRepository;
        private IGenericDataRepository<pdsetup> _pdsetupRepository;
        private IGenericDataRepository<packedgrade> _packedgradeRepository;
        private IGenericDataRepository<FarmerIncidentInspection> _farmerIncidentInspectionRepository;
        private IGenericDataRepository<AppPolicyApplication> _appPolicyApplicationRepository;
        private IGenericDataRepository<AppPolicyRole> _appPolicyRoleRepository;
        private IGenericDataRepository<AppPolicyUser> _appPolicyUserRepository;
        private IGenericDataRepository<AppPolicyUserRole> _appPolicyUserRoleRepository;

        public StecDBMSUnitOfWork()
        {
            _context = new StecDBMSEntities();
        }

        public IGenericDataRepository<pd> pdRepository
        {
            get
            {
                return _pdRepository ?? (_pdRepository = new GenericDataRepository<pd>(_context));
            }
        }

        public IGenericDataRepository<pdsetup> pdsetupRepository
        {
            get
            {
                return _pdsetupRepository ?? (_pdsetupRepository = new GenericDataRepository<pdsetup>(_context));
            }
        }

        public IGenericDataRepository<packedgrade> packedgradeRepository
        {
            get
            {
                return _packedgradeRepository ?? (_packedgradeRepository = new GenericDataRepository<packedgrade>(_context));
            }
        }

        public IGenericDataRepository<mat> matRepository
        {
            get
            {
                return _matRepository ?? (_matRepository = new GenericDataRepository<mat>(_context));
            }
        }

        public IGenericDataRepository<FarmerIncidentInspection> FarmerIncidentInspectionRepository
        {
            get
            {
                return _farmerIncidentInspectionRepository ?? (_farmerIncidentInspectionRepository = new GenericDataRepository<FarmerIncidentInspection>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyApplication> AppPolicyApplicationRepository
        {
            get
            {
                return _appPolicyApplicationRepository ?? (_appPolicyApplicationRepository = new GenericDataRepository<AppPolicyApplication>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyUser> AppPolicyUserRepository
        {
            get
            {
                return _appPolicyUserRepository ?? (_appPolicyUserRepository = new GenericDataRepository<AppPolicyUser>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyRole> AppPolicyRoleRepository
        {
            get
            {
                return _appPolicyRoleRepository ?? (_appPolicyRoleRepository = new GenericDataRepository<AppPolicyRole>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyUserRole> AppPolicyUserRoleRepository
        {
            get
            {
                return _appPolicyUserRoleRepository ?? (_appPolicyUserRoleRepository = new GenericDataRepository<AppPolicyUserRole>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
