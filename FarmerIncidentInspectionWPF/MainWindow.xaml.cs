﻿using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Home());
        }

        private void LogOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                user_setting.UserRoles = null;
                MainFrame.NavigationService.Navigate(new Form.Login());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InsepectionMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (user_setting.User == null)
                //{
                //    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    MainFrame.NavigationService.Navigate(new Form.Login());
                //    return;
                //}

                //if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                //    user_setting.UserRoles.Where(x => x.RoleName == "BlendingUser").Count() <= 0)
                //    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Inspection.BarcodeInspection());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageUserMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.UserAccount.Users());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InsepectionV2Menu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Inspection.BarcodeInspectionV2());
        }
    }
}
