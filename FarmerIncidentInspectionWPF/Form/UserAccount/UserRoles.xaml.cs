﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for UserRoles.xaml
    /// </summary>
    /// 

    public struct UserRoleDataGrid
    {
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class UserRoles : Window
    {
        AppPolicyUser _user;
        public UserRoles(AppPolicyUser user)
        {
            InitializeComponent();

            _user = new AppPolicyUser();
            _user = user;

            BindingUserRoleDataGrid();
        }

        private void BindingUserRoleDataGrid()
        {
            try
            {
                var list = from r in BusinessLayerServices.RoleBL().GetAll(user_setting.ApplicationID)
                           join u in BusinessLayerServices.UserRoleBL().GetByUsername(_user.Username, user_setting.ApplicationID)
                           on r.RoleID equals u.RoleID into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserRoleDataGrid
                           {
                               RoleID = r.RoleID,
                               RoleName = r.RoleName,
                               AssignStatus = rs == null ? false : true
                           };

                UserRoleDataGrid.ItemsSource = null;
                UserRoleDataGrid.ItemsSource = list.OrderBy(x => x.RoleName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UserRoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserRoleDataGrid.SelectedIndex < 0)
                    return;

                var model = (UserRoleDataGrid)UserRoleDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerServices.UserRoleBL()
                        .Add(new AppPolicyUserRole
                        {
                            Username = _user.Username,
                            RoleID = model.RoleID,
                            CreateDate = DateTime.Now,
                            CreateBy = user_setting.User.Username
                        }, user_setting.ApplicationID);
                else
                    BusinessLayerServices.UserRoleBL().Delete(_user.Username, model.RoleID);

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Roles window = new Roles();
                window.ShowDialog();

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
