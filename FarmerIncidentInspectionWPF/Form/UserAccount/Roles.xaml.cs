﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form.UserAccount
{
    /// <summary>
    /// Interaction logic for Roles.xaml
    /// </summary>
    public partial class Roles : Window
    {
        AppPolicyRole _role;
        public Roles()
        {
            InitializeComponent();
            DataGridBinding();
            _role = new AppPolicyRole();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (AppPolicyRole)RoleDataGrid.SelectedItem;
                BusinessLayerServices.RoleBL().Delete(model.RoleID);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                RoleNameTextBox.IsEnabled = false;

                _role = (AppPolicyRole)RoleDataGrid.SelectedItem;

                RoleNameTextBox.Text = _role.RoleName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                BusinessLayerServices.RoleBL().Add(new AppPolicyRole
                {
                    RoleID = Guid.NewGuid(),
                    RoleName = RoleNameTextBox.Text,
                    ApplicationID = user_setting.ApplicationID,
                    CreateBy = user_setting.User.Username,
                    CreateDate = DateTime.Now,
                    ModifiedBy = user_setting.User.Username,
                    ModifiedDate = DateTime.Now
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            RoleNameTextBox.Clear();
            AddButton.IsEnabled = true;
            EditButton.IsEnabled = false;
        }

        private void DataGridBinding()
        {
            try
            {
                RoleDataGrid.ItemsSource = null;
                RoleDataGrid.ItemsSource = BusinessLayerServices.RoleBL()
                    .GetAll(user_setting.ApplicationID)
                    .OrderBy(x => x.RoleName);

                TotalTextBlock.Text = RoleDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Role Name");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _role.RoleName = RoleNameTextBox.Text;
                _role.ModifiedBy = user_setting.User.Username;
                _role.ModifiedDate = DateTime.Now;

                BusinessLayerServices.RoleBL().Edit(_role);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
