﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
            UsernameTextBox.Focus();
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                LoginButton.IsEnabled = false;
                UsernameTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginProcess();
        }
        private void LoginProcess()
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Helper.ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ โปรดตรวจสอบกับทางแผนกไอที");

                if (Helper.ActiveDirectoryHelper.ActiveDirectoryAuthenticate(UsernameTextBox.Text, PasswordTextBox.Password) == false)
                    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                var userAccount = BusinessLayerServices.UserBL().GetSingle(UsernameTextBox.Text);
                if (userAccount == null)
                    throw new ArgumentException("บัญชีผู้ใช้งานของท่านยังไม่ได้ลงทะเบียนเข้าใช้งานระบบ โปรดติดต่อแผนกไอทีเพื่อทำการเพิ่มสิทธิ์การใช้งานระบบ");

                /// store login information.
                /// 
                user_setting.User = userAccount;
                user_setting.UserRoles = BusinessLayerServices.UserRoleBL()
                    .GetByUsername(userAccount.Username, user_setting.ApplicationID)
                    .Select(x => new AppPolicyRole
                    {
                        RoleID = x.RoleID,
                        RoleName = x.AppPolicyRole.RoleName
                    }).ToList();

                this.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            LoginProcess();
        }
    }
}
