﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form.Inspection
{
    /// <summary>
    /// Interaction logic for BarcodeInspectionV2.xaml
    /// </summary>
    public partial class BarcodeInspectionV2 : Page
    {
        public BarcodeInspectionV2()
        {
            InitializeComponent();
        }

        private void OnLoad()
        {
            OnClear();
        }

        private void OnClear()
        {
            Border1.Background = Brushes.White;
            BaleBarcodeTextBox.Clear();
            BaleBarcodeTextBox.Focus();
            FarmerIncidentDataGrid.ItemsSource = null;
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(BaleBarcodeTextBox.Text))
                    throw new Exception("โปรดสแกน Barcode");

                var greenBale = BusinessLayerServices.matBL()
                    .GetSingle(BaleBarcodeTextBox.Text);
                if (greenBale == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยารหัส barcode " + BaleBarcodeTextBox.Text + " นี้ในระบบ");

                IncidentListBinding(BaleBarcodeTextBox.Text);
                BaleBarcodeTextBox.SelectAll();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void IncidentListBinding(string baleBarcode)
        {
            var list = BusinessLayerServices.FarmerIncidentBL().GetByBaleBarcode(baleBarcode);
            FarmerIncidentDataGrid.ItemsSource = null;
            FarmerIncidentDataGrid.ItemsSource = list;

            if (list.Count() > 0)
                Border1.Background = Brushes.Salmon;
            else
                Border1.Background = Brushes.LightGreen;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            OnSearch();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            OnClear();
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                OnSearch();
        }
    }
}
