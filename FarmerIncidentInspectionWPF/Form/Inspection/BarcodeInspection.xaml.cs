﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionEntities;
using FarmerIncidentInspectionWPF.Form.Incident;
using FarmerIncidentInspectionWPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form.Inspection
{
    /// <summary>
    /// Interaction logic for BarcodeInspection.xaml
    /// </summary>
    public partial class BarcodeInspection : Page
    {
        public BarcodeInspection()
        {
            InitializeComponent();
            var crop = (short)DateTime.Now.Year;
            var pdnoList = BusinessLayerServices.pdsetupBL().GetByCrop(crop).OrderByDescending(o => o.pdno).ToList();
            PdnoCombobox.ItemsSource = pdnoList;
        }

        private void PdnoCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (PdnoCombobox.SelectedItem == null) return;
            BarcodeTextBox.Focus();
            BarcodeTextBox.Text = "";
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var pdno = (pdsetup)PdnoCombobox.SelectedItem;
                if (pdno == null) throw new Exception("กรุณาระบุ Production number!");

                var inspectionList = BusinessLayerServices.InspectionBL().GetByPdno(pdno.pdno).OrderByDescending(o => o.RecordDate);

                InspectionDataGrid.ItemsSource = null;
                InspectionDataGrid.ItemsSource = inspectionList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            var s = (FarmerIncidentInspection)InspectionDataGrid.SelectedItem;
            ViewIncidentDetail(s.BaleBarcode);
        }

        private void InspectionDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var s = (FarmerIncidentInspection)InspectionDataGrid.SelectedItem;
            ViewIncidentDetail(s.BaleBarcode);
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Click To Delete Inspection Detail
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetIncident();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            GetIncident();
        }

        private void GetIncident()
        {
            try
            {
                var pdno = (pdsetup)PdnoCombobox.SelectedItem;
                var bc = BarcodeTextBox.Text.Trim();
                if (pdno == null) throw new Exception("กรุณาระบุ Production number!");
                if (bc == "") throw new Exception("กรุณาระบุ Barcode!");

                var preCheck = BusinessLayerServices.matBL().GetSingle(bc);
                if (preCheck == null) throw new Exception("ไม่พบ Barcode ที่ระบุ กรุณาตรวจสอบ");

                var incidentList = BusinessLayerServices.FarmerIncidentBL().GetFromStoreByBaleBarcode(bc);
                var incidentAny = (incidentList != null && incidentList.Any());

                IncidentButton.IsEnabled = incidentAny;
                IncidentButton.Background = incidentAny ? Brushes.DarkOrchid : Brushes.Green;
                IncidentTexButton.Text = incidentAny ? "Incident Found" : "Incident Not Found";

                UpdateInspection(bc, pdno.pdno);
                BarcodeTextBox.Text = "";
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateInspection(string bc, string pdno)
        {
            var inspection = BusinessLayerServices.InspectionBL().GetSingle(bc);
            if (inspection == null)
            {
                BusinessLayerServices.InspectionBL().Add(new FarmerIncidentInspection
                {
                    BaleBarcode = bc,
                    pdno = pdno,
                    RecordDate = DateTime.Now,
                    //RecordBy = user_setting.User.Username
                    RecordBy = Environment.UserName
                });
            }
            else
            {
                inspection.pdno = pdno;
                inspection.RecordDate = DateTime.Now;
                //inspection.RecordBy = user_setting.User.Username;
                inspection.ActionFrom = "Farm Incident Inspection";
                inspection.RecordBy = Environment.UserName;
                BusinessLayerServices.InspectionBL().Update(inspection);
            }
        }

        private void IncidentButton_Click(object sender, RoutedEventArgs e)
        {
            ViewIncidentDetail(BarcodeTextBox.Text.Trim());
        }

        private void ViewIncidentDetail(string bc)
        {
            try
            {
                var pdno = (pdsetup)PdnoCombobox.SelectedItem;
                if (pdno == null) throw new Exception("กรุณาระบุ Production number!");
                if (bc == "") throw new Exception("กรุณาระบุ Barcode!");

                var preCheck = BusinessLayerServices.matBL().GetSingle(bc);
                if (preCheck == null) throw new Exception("ไม่พบ Barcode ที่ระบุ กรุณาตรวจสอบ");

                var incidentList = BusinessLayerServices.FarmerIncidentBL().GetFromStoreByBaleBarcode(bc);

                var w = new IncidentDetailWindow(bc, incidentList);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            BarcodeTextBox.Text = "";
        }
    }
}
