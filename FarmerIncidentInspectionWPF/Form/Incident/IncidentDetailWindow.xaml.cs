﻿using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FarmerIncidentInspectionWPF.Form.Incident
{
    /// <summary>
    /// Interaction logic for IncidentDetailWindow.xaml
    /// </summary>
    public partial class IncidentDetailWindow : Window
    {
        public IncidentDetailWindow()
        {
            InitializeComponent();
        }
        public IncidentDetailWindow(string bc, List<sp_GetIncidentFarmerByBaleBarcode_Result> incidentList)
        {
            try
            {
                InitializeComponent();
                HeaderTextblock.Text = string.Format("Incident Detail Barcode : {0}", bc);
                IncidentDataGrid.ItemsSource = incidentList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
