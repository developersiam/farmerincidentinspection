﻿using FarmerIncidentInspectionBL;
using FarmerIncidentInspectionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmerIncidentInspectionWPF.Helper
{
    public struct PackedGradeCrop
    {
        public short Crop { get; set; }
    }

    public struct TrueFalseStatus
    {
        public bool Status { get; set; }
        public string StatusName { get; set; }
    }

    public static class user_setting
    {
        public static List<PackedGradeCrop> Crops
        {
            get
            {
                List<PackedGradeCrop> list = new List<PackedGradeCrop>();
                foreach (var item in BusinessLayerServices.packedgradeBL()
                    .GetAll()
                    .GroupBy(x => x.crop)
                    .Select(x => new packedgrade { crop = x.Key }))
                {
                    list.Add(new PackedGradeCrop { Crop = Convert.ToInt16(item.crop) });
                }

                if (list.Where(x => x.Crop == DateTime.Now.Year).Count() <= 0)
                    list.Add(new PackedGradeCrop { Crop = Convert.ToInt16(DateTime.Now.Year) });

                return list;
            }
        }
        public static List<TrueFalseStatus> Status
        {
            get
            {
                List<TrueFalseStatus> list = new List<TrueFalseStatus>();
                list.Add(new TrueFalseStatus { Status = true, StatusName = "Finish" });
                list.Add(new TrueFalseStatus { Status = false, StatusName = "Not Finish" });

                return list;
            }
        }
        public static AppPolicyUser User { get; set; }
        public static List<AppPolicyRole> UserRoles { get; set; }
        public static Guid ApplicationID { get { return Guid.Parse("E83885E3-1249-4E1A-87AB-6DCB4E930B3A"); } }
    }
}
