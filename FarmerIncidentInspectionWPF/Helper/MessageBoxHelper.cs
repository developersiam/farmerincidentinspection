﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FarmerIncidentInspectionWPF.Helper
{
    public static class MessageBoxHelper
    {
        public static void Warning(string message)
        {
            MessageBox.Show(message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            return;
        }

        public static void Danger(string message)
        {
            MessageBox.Show(message, "error!!", MessageBoxButton.OK, MessageBoxImage.Error);
            return;
        }

        public static void Info(string message)
        {
            MessageBox.Show(message, "info", MessageBoxButton.OK, MessageBoxImage.Information);
            return;
        }

        public static MessageBoxResult Question(string message)
        {
            var result = MessageBox.Show(message, "please confirm!", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
                return MessageBoxResult.Yes;
            else
                return MessageBoxResult.No;
        }

        public static void Exception(Exception ex)
        {
            MessageBox.Show(ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            return;
        }
    }
}
